#dockerfile
FROM node:lts
ENV PORT $PORT
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install -s
COPY . ./
RUN npm run build
EXPOSE $PORT
CMD [ "node", "build/index.js" ]