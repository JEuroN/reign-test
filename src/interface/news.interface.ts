export interface News {
    story_title?: string,
    story_url?: string,
    author: string,
    _tags: string[],
    created_at: string,
    title?: string,
    url?: string,
    objectID: string,
}

export interface NewsRequest {
    field?: string,
    page?: number,
    keyword?: string
}