import config from './config';
import express from 'express';
import loadNews from './util/hackerWebApi';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from './util/JEuroN-reign-test-0.1-resolved.json';
import { getNews, delNews } from './controllers/news.controller';
import mongoose from 'mongoose';

mongoose
    .connect(config.dbUrl)
    .then(() => {
        console.log('Connected to db');
    })
    .catch((err) => {
        console.log(
            `MongoDB connection error. 
            Please make sure MongoDB is running. ${err}`,
        );
    });

const cors = require('cors');

const app: express.Express = express();
const router: express.Router = express.Router();

app.use(cors());
app.use(express.json());
app.use(router);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

router.get('/news', getNews);
router.put('/news/:id', delNews);

app.listen(config.port, () => loadNews);
