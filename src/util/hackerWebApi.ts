import axios from 'axios';
import {ToadScheduler, SimpleIntervalJob, AsyncTask} from 'toad-scheduler';
import {newsModel} from './mongo';
import {News} from '../interface/news.interface';

const scheduler: ToadScheduler = new ToadScheduler;
const getNews = async (): Promise<void> => {
  try {
    const {data: {hits}} = await axios.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs');

    hits.forEach(async (item: News) => {
      const newItem = {
        title: item.title || item.story_title,
        url: item.story_url || item.url,
        author: item.author,
        _tags: item._tags,
        created_at: item.created_at,
        _id: item.objectID,
      };

      await newsModel.findOneAndUpdate({_id: newItem._id},
          newItem, {upsert: true});
    });
  } catch (err: any) {
    console.log(err.message, err.stack);
  }
};

const task: AsyncTask = new AsyncTask('Get news', getNews, (err: Error) => {
  console.log(err);
});
const job: SimpleIntervalJob = new SimpleIntervalJob({hours: 1, runImmediately: true}, task);
const loadNews = scheduler.addSimpleIntervalJob(job);

export default loadNews;
