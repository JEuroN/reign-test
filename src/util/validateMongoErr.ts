export const validateMongo = (error: any) => {
    switch (error.code) {
    case 2:
        return { msg: 'Bad parameters on request', status: 400 };
    case 100:
        return { msg: 'Uncaught exception', status: 500 };
    case 51024:
        return { msg: 'Skip value must be > 0', status: 400 };
    default:
        return { msg: 'Internal error', status: 500 };
    }
};
