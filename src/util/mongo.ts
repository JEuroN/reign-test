import { Schema, model } from 'mongoose';

const news: Schema = new Schema({
    author: String,
    _tags: Array,
    created_at: String,
    title: String,
    url: String,
    _id: String,
    removed: { type: Boolean, default: false },
});

export const newsModel = model('news', news);
