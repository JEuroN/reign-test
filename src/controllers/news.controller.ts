import { Request, Response } from 'express';
import { NewsRequest } from '../interface/news.interface';
import { newsModel } from '../util/mongo';
import { validateMongo } from '../util/validateMongoErr';

export const getNews = async (
    req: Request,
    res: Response,
): Promise<Response> => {
    try {
        const { field = '', keyword = '', page = 0 } = req.query as NewsRequest;
        const newsResult = await newsModel
            .find({ removed: false, [field]: new RegExp(keyword, 'i') })
            .limit(5)
            .skip(5 * (page - 1));
        return res
            .status(newsResult.length ? 200 : 404)
            .json({ news: newsResult });
    } catch (err) {
        return res.json(validateMongo(err));
    }
};

export const delNews = async (
    req: Request,
    res: Response,
): Promise<Response> => {
    try {
        const newsResult = await newsModel.findOneAndUpdate(
            { _id: req.params.id },
            { $set: { removed: true } },
            { new: true },
        );
        return res.status(newsResult ? 200 : 404).json({ news: newsResult });
    } catch (err) {
        return res.json(validateMongo(err));
    }
};
