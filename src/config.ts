require('dotenv').config({ path: '.env' });

const config = {
    dbUrl: process.env.MONGODB || '',
    port: process.env.PORT || '',
};

export default config;

