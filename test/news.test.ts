import 'jest';
import request from 'supertest';
import { Express } from 'express-serve-static-core';
import express from 'express';
import { getNews, delNews } from '../src/controllers/news.controller';
import mongoose from 'mongoose';
import config from '../src/config';

let server: Express;

beforeAll(async () => {
    server = await express();
    server.use(express.json());
    mongoose.connect(config.dbUrl);
    server.listen(3000);
    server.post('/news', getNews);
    server.put('/news/:id', delNews);
});

afterAll((done) =>{
    mongoose.connection.close();
    done();
});

describe('GET REQUEST', () => {
    it('GET /NEWS', async () => {
        request(server)
            .get(`/news?field=title&keyword=show&page=1`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return err;
                expect(res.body).toMatchObject({ news: [] });
            });
    });

    it('GET /NEWS WITHOUT PARAMS', async () => {
        request(server)
            .get(`/news`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(200)
            .end((err, res) => {
                if (err) return err;
                expect(res.body).toMatchObject({ news: [] });
            });
    });

    it('GET /NEWS EXPECTING ERROR', async () => {
        request(server)
            .get(`/news?page=-1`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(400)
            .end((err, res) => {
                if (err) return err;
                expect(res.body).toMatchObject({ news: [] });
            });
    });
});

describe('PUT REQUEST', () => {
    it('PUT /NEWS WITHOUT ID', (done) => {
        request(server)
            .put(`/news/123`)
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(404)
            .end((err, res) => {
                if (err) return err;
                expect(res.body).toMatchObject({ news: null });
                done();
            });
    });
});
